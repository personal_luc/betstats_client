import axios from "axios";
import {
	FETCH_EVENTS,
	UPDATE_SESSION,
	SHOW_POPUP_LOADING,
	TOGGLE_POPUP,
	POPUPS
} from '../constants';

import {SERVER_URL} from "../utils/config";

const sessionState = isActive =>{
	return {
		type: UPDATE_SESSION,
		payload: {isActive}
	}
}


export function togglePopup(isActive, template)
{
	template = template || POPUPS.NOTEMPLATE;
	return {
		type: TOGGLE_POPUP,
		payload: {
			isActive: isActive,
			template
		}
	}
}

export function startSession(details)
{
	return dispatch => {
		axios.post(`${SERVER_URL}/api/start-session`,{...details})
			.then(response =>  {
				dispatch(sessionState(response.data));
				dispatch(togglePopup(false));
			})
			.catch(err => {
				console.log(err);
				dispatch(togglePopup(false));
			})
	}
}

export function endSession()
{
	return dispatch => {
		axios.post(`${SERVER_URL}/api/end-session`)
			.then(response =>  {
				dispatch(sessionState(response.data));
				dispatch(togglePopup(false));
			})
			.catch(err => {
				console.log(err);
				dispatch(togglePopup(false));
			})
	}
}

export function updateSessionState()
{
	return dispatch => {
		axios.post(`${SERVER_URL}/api/update-session`)
			.then(response =>  {
				dispatch(sessionState(response.data));
			})
			.catch(err => {
				console.log(err);
			})
	}

}

export function fetchEvents(dates){

	const theEvents = (data)=>{
		return {
			type: FETCH_EVENTS,
			payload: data
		}
	}
	const compare = (a,b)=>{
		return (a.dutchingPotential < b.dutchingPotential) ? -1 : ((b.dutchingPotential < a.dutchingPotential) ? 1 : 0); 
	}
	return dispatch => {
		return axios.post(`${SERVER_URL}/api/get-events`, dates)
			.then(({data: events}) => {
				events.sort(compare);
				dispatch(theEvents(events))
			})
			.catch(err => console.log(err))
	}
}

export function populateEvents(dates){
	return dispatch => {
		return axios.post(`${SERVER_URL}/api/populate-events`, dates)
			.then(()=>{ 
				dispatch({
					type: "POPULATE_EVENTS",
					payload: {}
				})
			})
	}
}



// 	handlePing()
// 	{
// 		MakeRequest({
// 			method: 'POST',
// 			url: `${SERVER_URL}/api/ping`,
// 			headers: {
// 				"Content-Type": "application/json"
// 			}
// 		})
// 		.then((response)=>{
// 			console.log(response);
// 		})
// 		.catch(function (err) {
// 			console.log(err);
// 		});
// 	}