import React from 'react';
import { Route, IndexRoute } from 'react-router';
import View from "./containers/View";
import ListEvents from "./containers/ListEvents";

export default (
  <Route path="/" component={View}>
    	<IndexRoute component={ListEvents} />
			
  </Route>
);

