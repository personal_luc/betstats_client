import React from "react";
import ReactDOM from "react-dom";
import { Router, browserHistory } from "react-router";
import { Provider } from "react-redux";
import * as actionCreators from './actions';
import store from "./store/index.js";
import { createHashHistory } from 'history';
import { syncHistoryWithStore, routerReducer } from 'react-router-redux';
import routes from './routes';

const history = syncHistoryWithStore(browserHistory, store)



ReactDOM.render(
	<Provider store={store}>
		<Router routes={routes} history={history} />
	</Provider>,
	document.querySelector(".bodyWrapper")
);