import React, {Component} from "react";
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actionCreators from '../../actions';
import store from "../../store/index.js";

import StartSessionForm from "../../components/StartSessionForm";
import PopulateEventsForm from "../../components/PopulateEventsForm";

class Header extends Component {

	componentDidMount()
	{
		store.dispatch(actionCreators.updateSessionState())	
	}
	
	togglePopup(template)
	{
		return this.props.bfsession.isActive ? 
		store.dispatch(actionCreators.endSession()) : 
		store.dispatch(actionCreators.togglePopup(true, template));
	}

	renderSessionButton()
	{
		let label = this.props.bfsession.isActive ? "End Session" : "Start Session";
		let classN = this.props.bfsession.isActive ? "btn-danger" : "btn-success";
		return <button className={`btn ${classN} align-self-end`} onClick={()=>(::this.togglePopup(<StartSessionForm />))}>{label}</button>
	}

	renderPopulateButton()
	{
		return this.props.bfsession.isActive ? 
		<button className="btn btn-primary mr-2" onClick={()=>( store.dispatch(actionCreators.togglePopup(true, <PopulateEventsForm />)) )}>Populate DB</button> :
		"";
	}

	// store.dispatch(actionCreators.populateEvents())

	render()
	{

		return (
				<div className="navbar navbar-dark bg-dark">
					<div className="col-3">
						<a className="navbar-brand" href="#">Betstats</a>
					</div>
					<div className="col-9 d-flex justify-content-end">
						{this.renderPopulateButton()}
						{this.renderSessionButton()}
					</div>
				</div>
		)
	}
}


const mapStateToProps = function(state) {
	return {	
		bfsession: state.bfsession
	}
};

const mapDispatchToProps = (dispatch) => ({
  actions : bindActionCreators(actionCreators, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);


// {this.renderPingButton()}