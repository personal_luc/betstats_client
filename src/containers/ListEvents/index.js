import React, {Component} from 'react';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actionCreators from '../../actions';
import store from "../../store/index.js";

import FetchEventsForm from "../../components/FetchEventsForm";
import RenderList from "../../components/ListEvents";


class ListEvents extends Component{	

	renderPage()
	{
		return (
			<div id="ListEvents" className="container-fluid">
				<FetchEventsForm submitAction={this.submitFetchEvents} />
				<hr />
				<RenderList events={this.props.events} />
			</div>
		)
	}

	renderNoSession()
	{
		return (
			<div id="ListEvents" className="container-fluid">
				<div className="row justify-content-md-center mt-4">
					<div className="col">
						<div className="alert alert-info" role="alert">
						  Session is not started..
						</div>
					</div>
				</div>
			</div>
		)	
	}

	submitFetchEvents(values)
	{
		return store.dispatch(actionCreators.fetchEvents(values))
	}

	render()
	{
		return this.props.bfsession.isActive ? this.renderPage() : this.renderNoSession();
	}
}

const mapStateToProps = state => { 
	return {
		events: state.events,
		bfsession: state.bfsession
	}; 
}

const mapDispatchToProps = (dispatch) => ({
  actions : bindActionCreators(actionCreators, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(ListEvents);