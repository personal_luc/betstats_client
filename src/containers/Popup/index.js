import React, {Component} from "react";
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actionCreators from '../../actions';
import store from "../../store/index.js";

class Popup extends Component{

	closePopup()
	{
		store.dispatch(actionCreators.togglePopup(false));
	}

	render()
	{
		let activeClass = this.props.popup.isActive ? "show" : "";
		let style = {display: this.props.popup.isActive ? "block":"none"};

		return (
			<div>
				<div className={`modal fade ${activeClass}`} id="popup" role="dialog" style={style}>
				  <div className="modal-dialog modal-lg" role="document">
				    <div className="modal-content">
				      <div className="modal-header">
				        <h5 className="modal-title" id="exampleModalLabel"></h5>
				        <button type="button" className="close" onClick={::this.closePopup}>
				          <span>&times;</span>
				        </button>
				      </div>
				      <div className="modal-body">
				        {this.props.popup.template}
				      </div>
				      <div className="modal-footer">
				        <button type="button" className="btn btn-secondary" onClick={::this.closePopup}>Close</button>
				      </div>
				    </div>
				  </div>
				</div>
				<div className={`modal-backdrop fade ${activeClass}`} style={style} onClick={::this.closePopup}></div>
			</div>

		)
	}
}

const mapStateToProps = function(state) {
	return {	
		popup: state.popup
	}
};

export default connect(mapStateToProps)(Popup);



