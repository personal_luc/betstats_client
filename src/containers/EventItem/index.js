import React, { Component } from "react";
import moment from "moment";

import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actionCreators from '../../actions';
import store from "../../store/index.js";

import EventDetails from "../../components/EventDetails";

class EventItem extends Component {

	placeBet()
	{
		console.log(this.props.event);
	}

	showDetails()
	{
		store.dispatch(actionCreators.togglePopup(true, <EventDetails event={this.props.event} onPlaceBet={::this.placeBet}/>));
	}

	renderItem({_id:key, name, openDate, countryCode, dutchingPotential = 0, runners})
	{
		return (
			<tr onClick={::this.showDetails}>
				<td>
					{name}
				</td>
				<td>
					{moment(openDate).format('dd, DD-MM-YYYY, HH:mm')}
				</td>
				<td>
					{countryCode}
				</td>
				<td>
					{parseFloat(dutchingPotential).toFixed(2)}
				</td>
			</tr>
		)
	}

	render()
	{
		return this.renderItem(this.props.event);
	}
}

const mapDispatchToProps = (dispatch) => ({
  actions : bindActionCreators(actionCreators, dispatch)
});

export default connect(null, mapDispatchToProps)(EventItem);