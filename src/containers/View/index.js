import React, {Component} from 'react';
import style from "./style.scss";

import Header from "../Header";
import Popup from "../Popup";

export default (props)=>{
	return (
		<div id="view">
			<Header />
			<main>
				{React.cloneElement(props.children, {props: props})}
			</main>
			<Popup />
		</div>
	)
}