import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import {browserHistory } from "react-router";
import { routerMiddleware } from 'react-router-redux'
import thunk from 'redux-thunk'; // allows us to use asynchronous actions
import rootReducer from "../reducers";

const middleware = routerMiddleware(browserHistory);

const createStoreWithMiddleware = compose(applyMiddleware(middleware, thunk), window.devToolsExtension ? window.devToolsExtension() : f => f)(createStore);
const store = createStoreWithMiddleware(rootReducer);

module.exports = store;