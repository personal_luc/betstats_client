// import {createConstants} from '../utils';
var constants = {
	FETCH_EVENTS: "FETCH_EVENTS",
	UPDATE_SESSION: "UPDATE_SESSION",
	SHOW_POPUP_LOADING: "SHOW_POPUP_LOADING",
	TOGGLE_POPUP: "TOGGLE_POPUP",
	POPUPS: {
		NOTEMPLATE: "NoTemplatePopup"
	}
};
module.exports = constants;