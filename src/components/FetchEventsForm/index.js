import React from "react";
import Datetime from 'react-datetime';
import style from "../../assets/style.scss";
import { Field, reduxForm } from 'redux-form'

const renderDTP = ({ dpvalid, input, name, label, type, meta: { touched, error, warning } })=>
{
	let invalidClass = touched && error ? "is-invalid" : "";
	return (
		<div className="col-4">
			<div className="form-group">
				<label> {label} </label>
				<Datetime {...input} isValidDate={ dpvalid } type={type}/>
			</div>
		</div>
	)
}

const valid = current => {
	return true;
	// let tomorrow  = Datetime.moment(new Date()).add(1,'days');
	// let yesterday = Datetime.moment(new Date()).add(-1, 'days');
    // return current.isAfter(yesterday) && current.isBefore(tomorrow);
};


const FetchEventsForm = props => {
	const { handleSubmit, pristine, reset, submitting } = props;
	return (
		<form className="form-inline row mt-4" onSubmit={handleSubmit(props.submitAction)}>
			<Field
				name="startingAfter" 
				label="Starting after:" 
				type="text" 
				component={renderDTP}
				dpvalid={valid}
			></Field>
			<Field
				name="startingBefore" 
				label="Starting before:" 
				type="text" 
				component={renderDTP}
				dpvalid={valid}
			></Field>
			<div className="col-4 d-flex align-items-center">
				<button className="btn btn-dark">Fetch events</button>
			</div>
		</form>
	)
}

export default reduxForm({
	form: 'FetchEventsForm'
})(FetchEventsForm)