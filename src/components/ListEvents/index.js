import React from "react";
import EventItem from "../../containers/EventItem";

const renderEvents = events => {
	return (
		<div className="col">
			<table className="table">
				<thead className="thead-inverse">
					<tr>
						<th>Event</th>
						<th>Date</th>
						<th>Country</th>
						<th>Dutching P.</th>
					</tr>
				</thead>
				<tbody>
					{events.map(event => <EventItem key={event._id} event={event}/>)}
				</tbody>
			</table>
		</div>
	)
}


const renderNoEvents = () => (
	<div className="col">
		<div className="alert alert-info" role="alert">
		  There are no events to display!
		</div>
	</div>
	)

export default ({events: {list: events}}) => {

	return <div className="row mt-4"> {events.length > 0 ? renderEvents(events) : renderNoEvents()} </div>
};