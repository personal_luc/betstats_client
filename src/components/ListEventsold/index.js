import React from "react";
// import MakeRequest from "../../utils/MakeRequest";
import {SERVER_URL} from "../../utils/config";
import uuid from "../../utils/uuid";
import { Link } from 'react-router'

export default class ListEvents extends React.Component{
	constructor(props)
	{
		super(props);
		this.state = {
			events : [],
			corrects: 0,
			wrongs: 0
		}
	}
	componentDidMount()
	{
		MakeRequest({
			method: 'POST',
			url: `${SERVER_URL}/api/computed`,
			headers: {
				"Content-Type": "application/json"
			},
			params: {
				// "active": true
			}
		})
		.then((response)=>{
			this.state.events = JSON.parse(response);
			this.forceUpdate();
		})
		.catch(function (err) {
			console.log(err);
		});
	}

	parseEntries(events)
	{
		return events
			.filter( entry => (Math.abs(parseFloat(entry.home_odds.diff)) > 5 && Math.abs(parseFloat(entry.away_odds.diff)) >5 ) &&
							(parseFloat(entry.home_odds.diff) < 0 || parseFloat(entry.away_odds.diff) < 0) )
			.map( event => {
				let isActive = (Date.now() < new Date(event.date).getTime());
				let home = `home: ${event.home_odds.diff}% (${event.home_odds.last}) `;
				let draw = `draw: ${event.draw_odds.diff}% (${event.draw_odds.last}) `;
				let away = `away: ${event.away_odds.diff}% (${event.away_odds.last}) `;

				let isCorrect = false;
				// let winnerOdd = !isActive ? event[`${event.winner}_odds`].last : "n/a";

				if( (event.home_odds.diff < 0 && event.winner === "home") ||
					(event.away_odds.diff < 0 && event.winner === "away") || 
					(event.draw_odds.diff < 0 && event.winner === "draw"))
				{
					isCorrect = true;
				}

				if(!isActive && isCorrect) this.state.corrects++;
				if(!isActive && !isCorrect) this.state.wrongs++;


				return <li data-active={isActive}  data-correct={isCorrect} key={uuid()}><Link to={`/event/${event.id}`}>{event.name}</Link> | {home}, {draw}, {away} | Winner: {event.winner}</li>;
			})
			 
	}

	render()
	{

		let events = this.parseEntries(this.state.events);
		return <ul className="events">
			Total: {this.state.corrects + this.state.wrongs} /
			Corecte: {this.state.corrects} / 
			Gresite: {this.state.wrongs}

			
			{events}
		</ul>
	}
}