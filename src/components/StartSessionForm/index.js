import React, {Component} from "react";
import { Field, reduxForm } from 'redux-form'
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actionCreators from '../../actions';
import store from "../../store/index.js";

const renderField = ({ input, name, label, type, meta: { touched, error, warning } }) =>
	{ 
		let invalidClass = touched && error ? "is-invalid" : "";
		return (
		<div className="form-group">
			<label for={name}> {label} </label>
			<input className={`form-control ${invalidClass}`} {...input} placeholder={label} id={name} type={type} />
			<div className="invalid-feedback">{touched ? error : ""}</div>
		</div>
		)
	}

const submit = (values)=> store.dispatch(actionCreators.startSession(values))

const StartSessionForm = props => {
	const { handleSubmit, pristine, reset, submitting } = props;

	return (
		<form onSubmit={handleSubmit(submit)}>
			<Field
				name="email" 
				label="Email" 
				type="email" 
				component={renderField} 
			></Field>
			<Field
				name="password"
				label="Password"
				type="password"
				component={renderField} 
			></Field>

			<button className="btn btn-primary" type="submit" disabled={pristine || submitting}>Submit</button>
		</form>
	)
}

const validate = values => {
  const errors = {}
  if (!values.email) {
    errors.email = 'Required'
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address'
  }
  if (!values.password) {
    errors.password = 'Required'
  }

  return errors
}

const mapDispatchToProps = (dispatch) => ({
  actions : bindActionCreators(actionCreators, dispatch)
});

export default reduxForm({
  validate,
  form: 'StartSessionForm' // a unique identifier for this form
})(
	connect(null, mapDispatchToProps)(StartSessionForm)
)


