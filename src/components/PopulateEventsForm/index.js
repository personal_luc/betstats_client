import React from "react";
import Datetime from 'react-datetime';
import style from "../../assets/style.scss";
import { Field, reduxForm } from 'redux-form'

import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actionCreators from '../../actions';
import store from "../../store/index.js";

const renderDTP = ({ dpdefault, dpvalid, input, name, label, type, meta: { touched, error, warning } })=>
{
	let invalidClass = touched && error ? "is-invalid" : "";
	return (
		<div className="form-group">
			<label> {label} </label>
			<Datetime {...input} defaultValue={dpdefault} isValidDate={ dpvalid } type={type}/>
		</div>
	)
}

const valid = current => {
	// let tomorrow  = Datetime.moment(new Date()).add(1,'days');
	let yesterday = Datetime.moment(new Date()).add(-1, 'days');
    return current.isAfter(yesterday);
};

const submit = (values)=> store.dispatch(actionCreators.populateEvents(values))


const PopulateEventsForm = props => {
	const { handleSubmit, pristine, reset, submitting } = props;
	return (
		<form className="mt-4" onSubmit={handleSubmit(submit)}>
			<Field
				name="startingAfter" 
				label="Starting after:" 
				type="text" 
				component={renderDTP}
				dpvalid={valid}
				dpdefault={Datetime.moment()}
			></Field>
			<Field
				name="startingBefore" 
				label="Starting before:" 
				type="text" 
				component={renderDTP}
				dpvalid={valid}
				dpdefault={Datetime.moment().add(2, 'h')}
			></Field>
			<div className="d-flex">
				<button className="btn btn-dark">Fetch events</button>
			</div>
		</form>
	)
}

const mapDispatchToProps = (dispatch) => ({
  actions : bindActionCreators(actionCreators, dispatch)
});

export default reduxForm({
	form: 'PopulateEventsForm'
})(
	connect(null, mapDispatchToProps)(PopulateEventsForm)
)