import React, { Component } from "react";
import uuid from "../../utils/uuid";
import moment from "moment";
import style from "./style.scss";

export default(props) => {
	const correctScoreKey = "Correct Score";
	const requiredScores = ["0-0", "0-1", "1-0"];
	const runners = Object.keys(props.event.runners);


	const showSelectionDetails = selections => {
		const factorClass = runnerName => {
			return Object.keys(props.event.dutchFactors).includes(runnerName) ? "isFactor" : "";
		}
		return (
			<td key={uuid()} className="runnerCol">
				<table className="table">
					<tbody>
						<tr>
							{selections.map(({runnerName}) => <td className={`selectionName ${factorClass(runnerName)}`} key={uuid()}>{runnerName}</td>)}
						</tr>
						<tr>
							{selections.map(({runnerName, lastPriceTraded}) => <td className={`selectionOdd ${factorClass(runnerName)}`} key={uuid()}>{lastPriceTraded}</td>)}
						</tr>
					</tbody>
				</table>
			</td>
		)
	}

	const clearSpace = str => str.replace(/\s/g, '');

	const showSelections = runner => {

		let selections = [];
		if(runner === correctScoreKey){
			selections = props.event.runners[correctScoreKey].filter(({runnerName}) => requiredScores.includes(clearSpace(runnerName) ) );
		}else{
			selections = props.event.runners[runner];
		}

		return showSelectionDetails(selections);
	}

	console.log(props.event);

	return (
		<table className="table">
			<thead>
				<tr>
					<td>{props.event.name}</td>
					<td>{moment(props.event.openDate).format('dd, DD-MM-YYYY, HH:mm')}</td>
					<td>{parseFloat(props.event.dutchingPotential).toFixed(2)}</td>
				</tr>
				<tr>
					{runners.map(key => <th key={uuid()}>{key}</th>)}
				</tr>
			</thead>
			<tbody>
				<tr>
					{runners.map(runner => showSelections(runner))}
				</tr>
				<tr>
					<td colSpan={3}>
						<button className="btn btn-dark" onClick={props.onPlaceBet}>Place Bet</button>
					</td>
				</tr>
			</tbody>

		</table>
		
	)
}


// <tr>
// 			<td className="p-0" colSpan="4">
// 				<table className="table table-sm">
					
// 					<tr>
// 						<td className="p-0" colSpan="4">
// 							<table className="table table-sm">
// 								<thead className="table-secondary">
// 									<tr>
// 										<th>Event</th>
// 										<th>Date</th>
// 										<th>Country</th>
// 									</tr>
// 								</thead>
// 								<tbody>
									
// 								</tbody>
// 							</table>
// 						</td>
// 					</tr>	
// 				</table>
// 			</td>
// 		</tr>