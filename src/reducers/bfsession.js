import {createReducer} from '../utils';
import {UPDATE_SESSION} from '../constants';

const initialState = {
	isActive: false,
	isWaiting: false
};

export default createReducer(initialState, {
	[UPDATE_SESSION]: (state, payload) => {
        return Object.assign({}, state, payload);
    }
});