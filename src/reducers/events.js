import {createReducer} from '../utils';
import {
	FETCH_EVENTS
} from '../constants';

const initialState = {
	list: []
};
export default createReducer(initialState, {
	[FETCH_EVENTS]: (state, payload) => {	
		return Object.assign({}, state, {
            'list': payload
        });
    }
});