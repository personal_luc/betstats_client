import {createReducer} from '../utils';
import {TOGGLE_POPUP,POPUPS} from '../constants';

const initialState = {
	isActive: false,
	template: POPUPS.NOTEMPLATE
};

export default createReducer(initialState, {
	[TOGGLE_POPUP]: (state, payload) => {
        return Object.assign({}, state, payload);
    }
});