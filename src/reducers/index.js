import {combineReducers} from 'redux';
import { routerReducer } from 'react-router-redux';
import bfsession from './bfsession';
import events from './events';
import popup from "./popupState";
import { reducer as reduxFormReducer } from 'redux-form';

export default combineReducers({
	events,
	bfsession,
	popup,
	form: reduxFormReducer,
	routing: routerReducer
})