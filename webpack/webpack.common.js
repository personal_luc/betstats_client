const path = require("path");

module.exports = {
  entry: path.join(__dirname, '../src/index.js'),
  devtool: 'cheap-module-source-map',
  output: {
    path: path.join(__dirname, '../build'),
    filename: 'bundle.js',
    sourceMapFilename: 'bundle.js.map'
  },
  module: {
    rules: [
      {
        test: /\.js?$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['es2015', 'react', 'stage-0']
          }
        }
      },
      {
        test: /\.scss/,
        use: ['style-loader', 
        {
          loader: 'css-loader',
        }, 'sass-loader']
      },
      {
        test: /\.(svg|jpe?g|png)$/,
        use: {
          loader: "file-loader",
          query: {
            "context": path.join(__dirname, "src"),
            "name": "assets/images/[name].[ext]"
          }
        }
      }
    ]
  }
}