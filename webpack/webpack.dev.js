const Merge = require('webpack-merge');
const CommonConfig = require('./webpack.common.js');
const path = require("path");
const webpack = require('webpack');

module.exports = Merge(CommonConfig, {
   devServer: {
    contentBase: path.join(__dirname, "/../build/"),
    port: 7777,
    host: '0.0.0.0',
    historyApiFallback: true,
    noInfo: false,
    stats: 'minimal',
    hot: true,
   disableHostCheck: true
  },
  plugins: [
  	new webpack.HotModuleReplacementPlugin()
  ]
})